i = 0
while i < 10 do
   i = i + 1
   print(i)
end


x = 0

repeat
   print(x)
   x = x + 1
until x == 10


print("TEST_FOR")

i = 5
print(i)
a = 1
b = 10

for test = a, b
do
   print(test)
end

print(i)

print("TEST_FOR_REVERSE")
for i = 10,1,-1
do
   print(i)
end


print("test_function")


function findIndex(ary, num)
   i = 1
   while ary[i] do
      if ary[i] == num then
         return i
      end

      i = i + 1
   end
end

print(findIndex({1, 5, 10}, 5))

i = 0
while true do
   i = i + 1
   if i == 10 then
      break
   end
end

print(i)

for i = 10, 20 do
   print("i = " .. i)
   if i % 3 == 0 and i % 5 == 0 then
      print("fifteen!")
      break
   end
end

a = 10
repeat
   a = a + 1
   print(a)
   if a == 15 then break end
until false
