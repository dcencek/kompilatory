if true then
   print("true")
end


if true then
   print("true")
else
   print("false")
end

if false then
else
   print("false")
end

if true then
   print("true")
else
end


x = 1

if x == 1 then
   x = 2
end

print(x)
