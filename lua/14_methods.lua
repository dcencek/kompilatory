person = { first_name = "Dominik", last_name = "Cencek" }

function person:full_name()
   return self.first_name .. " " .. self.last_name
end

print(person:full_name())
