Account = {balance = 0}

function Account:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function Account:deposit (v)
   self.balance = self.balance + v
end

function Account:withdraw (v)
   self.balance = self.balance - v
end


Account:deposit(1)

a1 = Account:new()
a2 = Account:new()
a1:deposit(10.0)

print(a1.balance)
print(a2.balance)

a1:withdraw(5.0)
print(a1.balance)
print(a2.balance)
