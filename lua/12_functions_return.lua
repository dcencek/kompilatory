function example(n)
   return 2 * 2
end

print(example(2))

function double(variable)
   return variable * 2
end

print(double(5))

value = 0
function factorial(num)
   if num == 1 then
      return 1
   end

   return num * factorial(num - 1)
end


function one()
   return 1
end

print("One: " .. one())

function sum(a, b)
   return a + b
end

print("sum(1, 2) = " .. sum(1, 2))
print('factorial_call_1')
print(factorial(1))
print('factorial_call_2')
print(factorial(2))
print('factorial_call_5')
print(factorial(5))
print('factorial_call_10')
print(factorial(10))

i = 0
function increment()
   i = i + 1
   return i
end

print(increment())
print(increment())
print(increment())
print(increment())

function fib(n)
   if n < 2 then
      return 1
   end

   return fib(n - 1) + fib(n - 2)
end

x = 0
while x < 10 do
   print('fib_' .. x)
   print(fib(x))
   x = x + 1
end


function multi_return()
   return 1, 2, 3, 4
end

a, b, c, d, e = multi_return()
print(a)
print(b)
print(c)
print(d)
print(e)


value = -one()
print(value)

function truefalse()
   return true, false
end

print(not truefalse())
print(truefalse())
print(multi_return())

i = 5
function example()
   local i = multi_return()
   print(i)
end

example()
print(i)
