function incr()
   local i
   i = 0
   return function()
      i = i + 1
      return i
   end
end

n1 = incr()
n2 = incr()

print(n1())
print(n2())
print(n1())
print(n1())
print(n1())

function multiplier(x)
   return function(y)
      return x * y
   end
end

double = multiplier(2)
triple = multiplier(3)

print(double(4))
print(double(10))
print(triple(4))
