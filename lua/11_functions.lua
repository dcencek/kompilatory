function globalIncrement(x)
   if j == nil then j = 0 end
   j = j + 1
   return j
end


print(globalIncrement())
print(globalIncrement())
print(globalIncrement())

function localIncrement(x)
   local j
   if j == nil then j = 0 end
   j = j + 1
   return j
end

print(localIncrement())
print(localIncrement())
print(localIncrement())


function hello(name)
   i = i + 1
   print("Hello " .. i .. " " .. name)
end

i = 0
hello("World")
hello("Test")

--------------------------

i = 5
print("starting i = " .. i)

function test_global_variable()
   i = 7
   while i < 10 do
      print(i)
      i = i + 1
   end
end

test_global_variable()
print("final i = " .. i)

--------------------------

i = 5
print("starting i = " .. i)

function test_local_variable()
   local i
   i = 7
   while i < 10 do
      print(i)
      i = i + 1
   end
end

test_local_variable()
print("final i = " .. i)


--------------------------

i = 5
print("starting i = " .. i)
print(j)

function test_local_variable()
   local i, j = 7, 9
   while i < 10 do
      print(i)
      i = i + 1
   end
end

print(j)

test_local_variable()
print("final i = " .. i)


function example()
   x = 5
   print("x = " .. x)
end

example()
print(x)
