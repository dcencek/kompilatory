squares = {1, 4, 9, 16, 25, 36, 49, 64, 81}

empty = {}

i = 0
while i <= 10 do
   print(squares[i])

   i = i + 1
end

print(empty[1])
empty[1] = "test"
print(empty[1])


foo = {{1, 2}, {4, 5}}
print(foo[1][1])
print(foo[1][2])
print(foo[2][1])
print(foo[2][2])

foo[2][2] = {9, 8, 7}
print(foo[2][2][1])
print(foo[2][2][2])
print(foo[2][2][3])

empty["z"] = "z"
print(empty["x"])
print(empty["z"])

print(empty.x)
print(empty.z)

print(empty.y)
empty.y = "test"
print(empty.y)
print(empty["y"])

key = "test"
abc = { [key] = key, foo = "bar", 123 }
print(abc[1])
print(abc.test)
print(abc.foo)
