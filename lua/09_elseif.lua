n = 0

if true then
   print("ZERO")
end

if true then
   print("zero")
elseif true then
   print("one")
end

-- if true then
--    print("zero")
-- elseif true then
-- end

if true then
   -- print("zero")
   print("Zero")
elseif true then
   print("foo") -- test
elseif true then
   print("foo")
elseif true then
   print("foo")
elseif true then
   print("foo")
end
--- test





if true then
   print("zero")
elseif true then
   print("foo")
elseif true then
   print("foo")
elseif true then
   print("foo")
else
   print("foo")
end

i = 0
while i < 10 do
   i = i + 1

   if i == 1 then
      print("ONE")
   elseif i == 2 then
   elseif i == 3 then
   else
      print("OTHER")
   end
end


if 10 == 0 then
   if 10 % 5 == 0 then
      if 10 % 2 == 0 then
         print("OK")
      end
   end
end
