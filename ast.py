import os

def get_value(ctx, obj):
    op = getattr(obj, "evaluate", None)
    if callable(op):
        return get_value(ctx, obj.evaluate(ctx))
    else:
        return obj

if 'DEBUG' in os.environ:
    if os.environ['DEBUG'] == "2":
        should_debug = 1

def debug(*arg):
    if 'DEBUG' in os.environ:
        print('[debug]', *arg)

# Context keeps variables and methods
class Context:
    def __init__(self, parent):
        self.parent = parent
        self.variables = {}

    def set_auto(self, key, value):
        "set_auto will set variable only if was previously set"
        if not self.parent or key in self.variables:
            self.set(key, value)
        else:
            self.parent.set_auto(key, value)


    def set(self, key, value):
        "set will set local variable"
        self.variables[key] = value


    def get(self, key):
        "get returns local variable or parent variable"
        if key in self.variables:
            return self.variables[key]
        elif self.parent:
            return self.parent.get(key)
        else:
            return None


# Implements binary operations (e.g 1 + 2)
class BinOp:
    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y

    def evaluate(self, ctx):
        x = get_value(ctx, self.x)
        if isinstance(x, list):
            x = x[0]

        y = get_value(ctx, self.y)
        if isinstance(y, list):
            y = y[0]


        if self.name == "+":
            return x + y

        if self.name == "-":
            return x - y

        if self.name == "/":
            return x / y

        if self.name == "*":
            return x * y

        if self.name == ">":
            return x > y

        if self.name == ">=":
            return x >= y

        if self.name == "<=":
            return x <= y

        if self.name == "<":
            return x < y

        if self.name == "~=":
            return x != y

        if self.name == "==":
            return x == y

        if self.name == "or":
            return x or y

        if self.name == "and":
            return x and y

        if self.name == "%":
            return x % y


        if self.name == "..":
            return to_lua_string(x) + to_lua_string(y)

        raise Exception("UNSUPPORTED BINOP " + self.name)

# Implements while loop
class While:
    def __init__(self, expr, block):
        self.expr = expr
        self.block = block
    def evaluate(self, ctx):
        try:
            while get_value(ctx, self.expr):
                get_value(ctx, self.block)
        except BreakException:
            pass

# Implements repat ... until loop
class Repeat:
    def __init__(self, block, expr):
        self.expr = expr
        self.block = block
    def evaluate(self, ctx):
        try:
            get_value(ctx, self.block)
        except BreakException:
            return

        if not get_value(ctx, self.expr):
            self.evaluate(ctx)

# Implements for loop
class For:
    def __init__(self, name, init, limit, step, block):
        self.name = name
        self.init = init
        self.limit = limit
        self.step = step
        self.block = block

    def evaluate(self, ctx):
        ctx = Context(ctx)

        step = get_value(ctx, self.step)
        init = get_value(ctx, self.init)
        limit = get_value(ctx, self.limit)

        var = init - step

        try:
            while True:
                var = var + step

                if step >= 0 and var > limit or step < 0 and var < limit:
                    return

                ctx.set(self.name, var)
                get_value(ctx, self.block)
        except BreakException:
            pass

# Implements conditional blocks
class If:
    def __init__(self, expr, if_true, if_false):
        self.expr = expr
        self.if_true = if_true
        self.if_false = if_false

    def evaluate(self, ctx):
        if get_value(ctx, self.expr) == True:
            get_value(ctx, self.if_true)
        else:
            get_value(ctx, self.if_false)

# Implements unary operations: '-123' and 'not true'
class UnOp:
    def __init__(self, name, x):
        self.name = name
        self.x = x

    def evaluate(self, ctx):
        value = get_value(ctx, self.x)
        if isinstance(value, list):
            value = value[0]

        if self.name == "-":
            return -value

        if self.name == "not":
            return not value

        raise Exception("UNSUPPORTED UNOP " + self.name)


def to_lua_string(value):
    if isinstance(value, bool):
        if value == True:
            return "true"
        if value == False:
            return "false"
    if value == None:
        return "nil"

    return str(value)

class ReturnException(Exception):
    def __init__(self, value):
        self.v = value

class Return:
    def __init__(self, expr):
        self.expr = expr

    def evaluate(self, ctx):
        values = list(map(lambda x: get_value(ctx, x), self.expr))
        raise ReturnException(values)

class BreakException(Exception):
    pass

class Break:
    def evaluate(self, ctx):
        raise BreakException()

class Lambda:
    def __init__(self, args, block):
        self.args = args
        self.block = block

    def evaluate(self, ctx):
        return BoundLambda(ctx, self.args, self.block)

class BoundLambda:
    def __init__(self, ctx, args, block):
        self.ctx = ctx
        self.args = args
        self.block = block

    def _call(self, ctx, receiver, args):
        newCtx = Context(self.ctx)

        # Set self value
        newCtx.set("self", receiver)

        # Set local variables to nil
        for arg in self.args:
            newCtx.set(arg, None)

        # Set local variables to valid values
        for i, arg in enumerate(args):
            newCtx.set(self.args[i], arg)

        try:
            return self.block.evaluate(newCtx)
        except ReturnException as e:
            return e.v

# Symbol represents dynamic identifier
class Symbol:
    def __init__(self, name):
        self.name = name

    def receiver(self):
        return self

    def evaluate(self, ctx):
        return ctx.get(self.name)

    def _set(self, ctx, value):
        ctx.set_auto(self.name, value)

    def _set_local(self, ctx, value):
        ctx.set(self.name, value)

# Implements: a[0][1]
class TableField:
    def __init__(self, table, name):
        self.table = table
        self.name = name

    def receiver(self):
        return self.table

    def evaluate(self, ctx):
        table = get_value(ctx, self.table)
        key = get_value(ctx, self.name)
        return table.get(key)

    def _set(self, ctx, value):
        table = get_value(ctx, self.table)
        key = get_value(ctx, self.name)
        table.set(key, value)

    def _set_local(self, ctx, value):
        return self._set(ctx, value)


# Implements: a, b = 1, 2
class Assign:
    def __init__(self, var, expr):
        self.var = var
        self.expr = expr

    def evaluate(self, ctx):
        idx = 0

        for expr in self.expr:
            values = get_value(ctx, expr)

            if not isinstance(values, list):
                values = [values]

            for v in values:
                if len(self.var) > idx:
                    self.set_value(self.var[idx], ctx, v)
                    idx += 1

        while len(self.var) > idx:
            self.set_value(self.var[idx], ctx, None)
            idx += 1

    def set_value(self, variable, ctx, value):
        variable._set(ctx, value)


# local x = 1
class LocalAssign(Assign):
    def set_value(self, variable, ctx, value):
        variable._set_local(ctx, value)


class FunctionCall:
    def __init__(self, symbol, args):
        self.symbol = symbol
        self.args = args

    def evaluate(self, ctx):
        unsafeArgs = list(map(lambda x: get_value(ctx, x), self.args))

        # Works similar like flatten
        args = []
        for arg in unsafeArgs:
            if isinstance(arg, list):
                for x in arg:
                    args.append(x)
            else:
                args.append(arg)

        fn = get_value(ctx, self.symbol)

        if fn:
            return fn._call(ctx, self.symbol.receiver(), args)

        raise Exception("Cannot call nil function ", self.symbol.name)


class CodeBlock:
    def __init__(self):
        self.nodes = []

    def append(self, expr):
        self.nodes.append(expr)
        return self

    def evaluate(self, ctx):
        for node in self.nodes:
            get_value(ctx, node)

        return None

class Noop:
    def evaluate(self, ctx):
        return None

class Interpreter:
    class FnWrapper:
        def __init__(self, fn):
            self.fn = fn

        def _call(self, _ctx, _receiver, args):
            self.fn(args)

    def __init__(self, stdout):
        self.stdout = stdout

    def start(self, root):
        ctx = Context(None)

        ctx.set("print", self.FnWrapper(self._print))
        ctx.set("setmetatable", self.FnWrapper(self._setmetatable))

        get_value(ctx, root)

    # Builtin functions

    def _print(self, args):
        self.stdout.write(
            "\t".join(map(to_lua_string, args)) + "\n"
        )

    def _setmetatable(self, args):
        return args[0].setmetatable(args[1])


# Table implements array and dictionary
class Table:
    def __init__(self):
        self.t = {}
        self.metatable = None

    def get(self, key):
        try:
            return self.t[key]
        except KeyError:
            if self.metatable:
                return self.metatable.get(key)

            return None

    def set(self, key, value):
        self.t[key] = value

    def setmetatable(self, metatable):
        self.metatable = metatable

# FieldList is useful for constructing new table
class Fieldlist:
    ARRAY = "__array_item__"
    def __init__(self, fields):
        self.fields = fields

    def evaluate(self, ctx):
        counter = 0

        table = Table()

        for field in self.fields:
            if field[0] == self.ARRAY:
                counter += 1
                table.set(counter, get_value(ctx, field[1]))
            else:
                key = get_value(ctx, field[0])
                table.set(key, get_value(ctx, field[1]))

        return table
