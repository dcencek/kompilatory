#!/usr/bin/env python3
import os, sys

sys.path.insert(0, "./")

import ast

precedence = (
    ('left', 'OR'),
    ('left', 'AND'),
    ('left', 'LT', 'GT', 'EQ_LT', 'EQ_GT', 'NEQUALS', 'EQUALS'),
    ('left', 'CONCAT'),
    ('left', 'TIMES', 'DIVIDE', 'MODULO'),
)

reserved = {
    'if' : 'IF',
    'while' : 'WHILE',
    'elseif' : 'ELSEIF',
    'else' : 'ELSE',
    'then' : 'THEN',
    'false' : 'FALSE',
    'for': 'FOR',
    'true' : 'TRUE',
    'nil': 'NIL',
    'local': 'LOCAL',
    'end': 'END',
    'function': 'FUNCTION',
    'not': 'NOT',
    'and': 'AND',
    'or': 'OR',
    'do': 'DO',
    'return': 'RETURN',
    'break': 'BREAK',
    'repeat': 'REPEAT',
    'until': 'UNTIL',
}

tokens = [
    'ASSIGN',
    'CONCAT',
    'COMMENT',
    'DIVIDE',
    'EQUALS',
    'EQ_GT',
    'EQ_LT',
    'GT',
    'LBRACE',
    'LBRACKET',
    'LPAREN',
    'LT',
    'MINUS',
    'NAME',
    'NEQUALS',
    'NUMBER',
    'PLUS',
    'RBRACE',
    'RBRACKET',
    'RPAREN',
    'SEMICOLON',
    'STRING',
    'TIMES',
    'MODULO',
    'COMMA',
    'DOT',
    'COLON',
] + list(reserved.values())

# Signs
t_MODULO    = r'%'
t_LPAREN    = r'\('
t_RPAREN    = r'\)'
t_LBRACE    = r'{'
t_RBRACE    = r'}'
t_LBRACKET  = r'\['
t_RBRACKET  = r'\]'
t_SEMICOLON = r';'
t_COLON     = r':'
t_PLUS      = r'\+'
t_ASSIGN    = r'='
t_CONCAT    = r'\.\.'
t_MINUS     = r'-'
t_TIMES     = r'\*'
t_DIVIDE    = r'/'
t_EQUALS    = r'=='
t_LT        = r'<'
t_EQ_LT     = r'<='
t_GT        = r'>'
t_EQ_GT     = r'>='
t_COMMA     = r','
t_DOT       = r'\.'
t_NEQUALS   = r'\~='

# Keywords
t_IF       = r"if"
t_ELSEIF   = r"elseif"
t_END      = r"end"
t_ELSE     = r"else"
t_THEN     = r"then"
t_NOT      = r'not'
t_FALSE    = r"false"
t_NIL      = r"nil"
t_TRUE     = r"true"
t_FUNCTION = r"function"

def t_NAME(t):
    r'[a-zA-Z_]+([a-zA-Z0-9_]*)'
    t.type = reserved.get(t.value, 'NAME')
    return t

t_ignore = " \n"

should_debug = 0

lua = ast.Interpreter(sys.stdout)

if 'DEBUG' in os.environ:
    if os.environ['DEBUG'] == "1":
        should_debug = 1

def debug(*arg):
    if 'DEBUG' in os.environ:
        print('[DEBUG]', *arg)

def t_NUMBER(t):
    r'(\d+\.\d+)|(\d+)'

    if "." in t.value:
        t.value = float(t.value)
    else:
        t.value = int(t.value)

    return t

def t_STRING(t):
    r'(\'.*\')|(\"([^"]*)\")'
    t.value = t.value[1:-1]
    return t

def t_COMMENT(t):
    r'--.*'
    return t

def t_newline(t):
    r'\n'
    t.lexer.lineno += t.value.count("\n")


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

import ply.lex as lex
lexer = lex.lex(debug = should_debug)

last_block = 1

def p_block(p):
    "block : statement"

    global last_block
    last_block = p

    p[0] = ast.CodeBlock().append(p[1])


def p_block_append(p):
    "block : block statement"
    p[0] = p[1].append(p[2])

def p_statement_with_comment(p):
    'statement : statement COMMENT'
    debug("statement_with_comment", p[1], p[2])
    p[0] = p[1]

def p_statement_comment(p):
    'statement : COMMENT'
    debug("statement_comment")
    p[0] = ast.Noop()

def p_statement_semicolon(p):
    'statement : SEMICOLON'
    debug("statement_semicolon", p[0])
    p[0] = ast.Noop()

def p_expression_functiondef(p):
    'expression : functiondef'
    debug("statement_functiondef")
    p[0] = p[1]

def p_expression_functioncall(p):
    'expression : functioncall'
    p[0] = p[1]

def p_stat_expr(p):
    "statement : expression"
    p[0] = p[1]

def p_return(p):
    "statement : RETURN expressionlist"
    p[0] = ast.Return(p[2])

def p_break(p):
    "statement : BREAK"
    p[0] = ast.Break()

def p_statement_if_else(p):
    "statement : IF expression statement_then statement_else"

    debug('p_statement_if_else')
    p[0] = ast.If(p[2], p[3], p[4])

def p_statement_then(p):
    '''
    statement_then : THEN block
                   | THEN
    '''

    if len(p) == 3:
        p[0] = p[2]
    else:
        p[0] = ast.Noop()

def p_statement_else(p):
    '''
    statement_else : ELSE block END
                   | ELSEIF expression statement_then statement_else
                   | END
                   | ELSE END
    '''


    debug('p_statement_else')
    if len(p) == 5:
        p[0] = ast.If(p[2], p[3], p[4])
    elif len(p) == 4:
        p[0] = p[2]
    else:
        p[0] = ast.Noop()

def p_statement_while(p):
    'statement : WHILE expression DO block END'
    p[0] = ast.While(p[2], p[4])

def p_statement_repeat(p):
    'statement : REPEAT block UNTIL expression'
    p[0] = ast.Repeat(p[2], p[4])

def p_statement_for_numeric(p):
    'statement : FOR NAME ASSIGN expression COMMA expression DO block END'
    p[0] = ast.For(p[2], p[4], p[6], 1, p[8])

def p_statement_for_numeric_step(p):
    'statement : FOR NAME ASSIGN expression COMMA expression COMMA expression DO block END'
    p[0] = ast.For(p[2], p[4], p[6], p[8], p[10])

def p_functiondef(p):
    'functiondef : FUNCTION funcname parameters block END'
    debug('functiondef')
    p[0] = ast.Assign([p[2]], [ast.Lambda(p[3], p[4])])

def p_lambda(p):
    'functiondef : FUNCTION parameters block END'
    debug('LAMBDA')
    p[0] = ast.Lambda(p[2], p[3])

def p_funcname_object(p):
    'funcname : NAME COLON NAME'
    debug("funcname", list(p))
    p[0] = ast.TableField(ast.Symbol(p[1]), p[3])

def p_funcname(p):
    'funcname : NAME'
    p[0] = ast.Symbol(p[1])

def p_parameters(p):
    "parameters : LPAREN parlist RPAREN"
    p[0] = p[2]

def p_parameters_empty(p):
    "parameters : LPAREN RPAREN"
    p[0] = []

def p_table(p):
    "table : LBRACE fieldlist RBRACE"
    p[0] = ast.Fieldlist(p[2])

def p_tablefield_expr(p):
    "tablefield : expression"
    p[0] = (ast.Fieldlist.ARRAY, p[1])

def p_tablefield_assign_expr(p):
    "tablefield : NAME ASSIGN expression"
    p[0] = (p[1], p[3])

def p_tablefield_assign_dynamic(p):
    "tablefield : LBRACKET expression RBRACKET ASSIGN expression"
    p[0] = (p[2], p[5])

def p_table_empty(p):
    "table : LBRACE RBRACE"
    p[0] = ast.Fieldlist([])

def p_fieldlist_append(p):
    "fieldlist : fieldlist COMMA tablefield"
    p[1].append(p[3])
    p[0] = p[1]

def p_fieldlist_one(p):
    "fieldlist : tablefield"
    p[0] = [p[1]]


def p_expression_table(p):
    "expression : table"
    p[0] = p[1]

def p_functioncall(p):
    'functioncall : funcname arguments'
    p[0] = ast.FunctionCall(p[1], p[2])
    debug("p_functioncall", list(p))

def p_arguments(p):
    "arguments : LPAREN expressionlist RPAREN"
    p[0] = p[2]

def p_arguments_empty(p):
    "arguments : LPAREN RPAREN"
    p[0] = []

def p_parlist(p):
    "parlist : parlist COMMA NAME"
    p[1].append(p[3])
    p[0] = p[1]

def p_parlist_empty(p):
    "parlist : NAME"
    p[0] = [p[1]]

def p_expression_nil(p):
    "expression : NIL"
    debug("expression_nil")
    p[0] = None

def p_expression_true(p):
    "expression : TRUE"
    debug("p_expression_true")
    p[0] = True

def p_expression_false(p):
    "expression : FALSE"
    debug("p_expression_false")
    p[0] = False

def p_expression_var(p):
    "expression : var"
    p[0] = p[1]

def p_expression_number(p):
    "expression : NUMBER"
    p[0] = p[1]

def p_expression_string(p):
    "expression : STRING"
    p[0] = p[1]

def p_var(p):
    'var : NAME'
    p[0] = ast.Symbol(p[1])

def p_var_table(p):
    "var : var LBRACKET expression RBRACKET"
    p[0] = ast.TableField(p[1], p[3])

def p_var_member(p):
    "var : var DOT NAME"
    p[0] = ast.TableField(p[1], p[3])

def p_varlist(p):
    "varlist : varlist COMMA var"
    p[1].append(p[3])
    p[0] = p[1]

def p_varlist_empty(p):
    "varlist : var"
    p[0] = [p[1]]

def p_expressionlist(p):
    "expressionlist : expressionlist COMMA expression"
    p[1].append(p[3])
    p[0] = p[1]

def p_expressionlist_empty(p):
    "expressionlist : expression"
    p[0] = [p[1]]

def p_statement_local_declare(p):
    'statement : LOCAL varlist'
    p[0] = ast.LocalAssign(p[2], [])

def p_statement_local_assign(p):
    'statement : LOCAL varlist ASSIGN expressionlist'
    p[0] = ast.LocalAssign(p[2], p[4])

def p_statement_assign(p):
    'statement : varlist ASSIGN expressionlist'
    p[0] = ast.Assign(p[1], p[3])

def p_expression_binop(p):
    '''
    expression : expression OR expression
               | expression AND expression
               | expression NEQUALS expression
               | expression EQUALS expression
               | expression EQ_GT expression
               | expression GT expression
               | expression EQ_LT expression
               | expression LT expression
               | expression CONCAT expression
               | expression MODULO expression
               | expression DIVIDE expression
               | expression TIMES expression
               | expression MINUS expression
               | expression PLUS expression
    '''
    p[0] = ast.BinOp(p[2], p[1], p[3])

def p_expression_unop(p):
    '''
    expression : NOT expression
               | MINUS expression
    '''
    p[0] = ast.UnOp(p[1], p[2])



def p_error(p):
    if p:
        print("Syntax error:", p)
        #import code; code.interact(local=dict(globals(), **locals()))

    else:
        print("Syntax error at EOF")

import ply.yacc as yacc
parser = yacc.yacc()

import sys

if len(sys.argv) < 2:
    print("USAGE: " + sys.argv[0] + " file.lua")
    sys.exit(1)

file = open(sys.argv[1])
code = file.read()
file.close()

parsed = parser.parse(code, debug = should_debug)

lua.start(last_block[0])
