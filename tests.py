#!/usr/bin/env python3

import unittest
import subprocess
import os
import distutils.spawn

class TestLuaFiles(unittest.TestCase):
    pass

def add_testcase(file):
    def testcase(self):
        print(file)
        cmd = ""
        if distutils.spawn.find_executable("lua5.3"):
            cmd = "lua5.3"
        elif  distutils.spawn.find_executable("lua"):
            cmd = "lua"
        else:
            self.fail("lua not found")

        out1 = subprocess.check_output([cmd, "lua/" + file])
        out2 = subprocess.check_output(["./lua.py", "lua/" + file])
        self.assertEqual(out1, out2)


    testcase.__name__ = "test_%s" % file
    setattr(TestLuaFiles, testcase.__name__, testcase)

for file in os.listdir("lua"):
    add_testcase(file)

if __name__ == '__main__':
    unittest.main()
